import os
import json
from concerto_2_chrono_format import ConcertoFormatter


def test_formatting():
    formatter = ConcertoFormatter('data.csv')
    formatter.format()

    with open('concerto.json', 'rb') as fp:
        data = json.load(fp)

    agendas = data['agendas']
    assert len(agendas) == 2

    for agenda in agendas:
        if agenda['slug'] == 'crche-collective-du-baron':
            assert agenda['kind'] == 'events'
            assert agenda['permissions']['edit'] is None
            assert agenda['permissions']['view'] is None
            assert len(agenda['events']) == 4
            for event in agenda['events']:
                if event['places'] == 0:
                    assert event['start_datetime'] in ("2017-03-14 15:00:00",
                                                       "2017-03-14 15:30:00")
                else:
                    assert event['start_datetime'] in ("2017-03-13 16:00:00",
                                                       "2017-03-13 16:30:00")
        else:
            assert agenda['kind'] == 'events'
            assert agenda['permissions']['edit'] is None
            assert agenda['permissions']['view'] is None
            assert len(agenda['events']) == 4
            for event in agenda['events']:
                if event['places'] == 5:
                    assert event['start_datetime'] in ("2017-03-14 10:00:00",
                                                       "2017-03-14 10:30:00")
                else:
                    assert event['start_datetime'] in ("2017-03-14 13:00:00",
                                                       "2017-03-14 13:30:00")


def teardown_module(module):
    os.remove('concerto.json')
